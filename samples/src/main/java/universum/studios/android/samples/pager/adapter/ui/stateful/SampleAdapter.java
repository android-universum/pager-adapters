/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.ui.stateful;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.content.Loader;
import universum.studios.android.database.adapter.FragmentPagerCursorLoaderAdapter;
import universum.studios.android.database.annotation.LoaderId;
import universum.studios.android.samples.pager.adapter.data.Loaders;
import universum.studios.android.samples.pager.adapter.data.model.Page;
import universum.studios.android.samples.pager.adapter.ui.PageFragment;

/**
 * @author Martin Albedinsky
 */
@LoaderId(Loaders.PAGES)
final class SampleAdapter extends FragmentPagerCursorLoaderAdapter<Page.CursorWrapper, Page> {

	private final Context context;

	SampleAdapter(@NonNull final FragmentActivity context) {
		super(context);
		this.context = context;
	}

	@Override @NonNull public Loader<Cursor> createLoader(final int loaderId, @NonNull final Bundle params) {
		return Loaders.createPagesLoader(context);
	}

	@Override @NonNull public Page.CursorWrapper wrapCursor(@NonNull final Cursor cursor) {
		return Page.CursorWrapper.wrap(cursor);
	}

	@Override @NonNull protected Fragment onGetItem(@NonNull final Page.CursorWrapper pages, final int position) {
		return PageFragment.newInstance(pages.getItem());
	}

	@Override public int getItemPosition(@NonNull final Object object) {
		final Page.CursorWrapper pages = getCursor();
		if (pages != null && pages.moveToFirst()) {
			final long pageId = ((PageFragment) object).getPageId();
			do {
				if (pageId == pages.getId()) {
					return pages.getPosition();
				}
			} while (pages.moveToNext());
		}
		return POSITION_NONE;
	}
}