/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.pager.adapter;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.junit.Test;
import org.robolectric.annotation.Config;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class FragmentPagerAdapterTest extends AndroidTestCase {

	private static final int CONTAINER_ID = android.R.id.list;

	private ViewGroup container;

	@Override public void beforeTest() {
		super.beforeTest();
		this.container = new FrameLayout(context());
		this.container.setId(CONTAINER_ID);
	}

	@Override public void afterTest() {
		super.afterTest();
		this.container = null;
	}

	@Test public void testConstants() {
		// Assert:
		assertThat(FragmentPagerAdapter.NO_POSITION, is(-1));
		assertThat(FragmentPagerAdapter.NO_ID, is(-1L));
	}

	@Test public void testStartUpdate() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act:
		adapter.startUpdate(container);
	}

	@Test(expected = IllegalStateException.class)
	public void testStartUpdateWithViewWithoutId() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act:
		adapter.startUpdate(new FrameLayout(context()));
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testInstantiateItemBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testInstantiateItemAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareInstantiateItemTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		// Act:
		final Fragment fragment = (Fragment) adapter.instantiateItem(container, 0);
		// Assert:
		final String fragmentTag = adapter.makeItemTag(CONTAINER_ID, adapter.getItemId(0));
		assertThat(fragment, is(notNullValue()));
		verify(mockFragmentManager).beginTransaction();
		verify(mockFragmentManager).findFragmentByTag(fragmentTag);
		verify(mockTransaction).add(CONTAINER_ID, fragment, fragmentTag);
		verify(mockTransaction, times(0)).attach(any(Fragment.class));
		return mockTransaction;
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testInstantiateItemMultipleBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemMultipleTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testInstantiateItemMultipleAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemMultipleTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareInstantiateItemMultipleTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		// Act:
		final Fragment fragment1 = (Fragment) adapter.instantiateItem(container, 0);
		final Fragment fragment2 = (Fragment) adapter.instantiateItem(container, 1);
		// Assert:
		final String fragmentTag1 = adapter.makeItemTag(CONTAINER_ID, adapter.getItemId(0));
		final String fragmentTag2 = adapter.makeItemTag(CONTAINER_ID, adapter.getItemId(1));
		assertThat(fragment1, is(notNullValue()));
		assertThat(fragment2, is(notNullValue()));
		assertThat(fragment1, is(not(fragment2)));
		verify(mockFragmentManager).beginTransaction();
		verify(mockFragmentManager).findFragmentByTag(fragmentTag1);
		verify(mockFragmentManager).findFragmentByTag(fragmentTag2);
		verify(mockTransaction).add(CONTAINER_ID, fragment1, fragmentTag1);
		verify(mockTransaction).add(CONTAINER_ID, fragment2, fragmentTag2);
		verify(mockTransaction, times(0)).attach(any(Fragment.class));
		return mockTransaction;
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testInstantiateItemAlreadyAddedBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemAlreadyAddedTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testInstantiateItemAlreadyAddedAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemAlreadyAddedTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareInstantiateItemAlreadyAddedTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(mockFragmentManager);
		final String fragmentTag = adapter.makeItemTag(CONTAINER_ID, adapter.getItemId(0));
		when(mockFragmentManager.findFragmentByTag(fragmentTag)).thenReturn(mockFragment);
		// Act:
		final Fragment fragment = (Fragment) adapter.instantiateItem(container, 0);
		// Assert:
		assertThat(fragment, is(mockFragment));
		verify(mockFragmentManager).beginTransaction();
		verify(mockFragmentManager).findFragmentByTag(fragmentTag);
		verify(mockTransaction).attach(fragment);
		verify(mockTransaction, times(0)).add(anyInt(), any(Fragment.class), anyString());
		verify(mockFragment).setMenuVisibility(false);
		verify(mockFragment).setUserVisibleHint(false);
		return mockTransaction;
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testInstantiateItemThatIsPrimaryBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemThatIsPrimaryTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testInstantiateItemThatIsPrimaryAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemThatIsPrimaryTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareInstantiateItemThatIsPrimaryTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment = mock(Fragment.class);
		adapter.setPrimaryItem(container, 0, mockFragment);
		final String fragmentTag = adapter.makeItemTag(CONTAINER_ID, adapter.getItemId(0));
		when(mockFragmentManager.findFragmentByTag(fragmentTag)).thenReturn(mockFragment);
		// Act:
		final Fragment fragment = (Fragment) adapter.instantiateItem(container, 0);
		// Assert:
		assertThat(fragment, is(mockFragment));
		verify(mockFragmentManager).beginTransaction();
		verify(mockFragmentManager).findFragmentByTag(fragmentTag);
		verify(mockTransaction).attach(fragment);
		verify(mockTransaction, times(0)).add(anyInt(), any(Fragment.class), anyString());
		verify(mockFragment, times(0)).setMenuVisibility(false);
		verify(mockFragment, times(0)).setUserVisibleHint(false);
		return mockTransaction;
	}

	@Test public void testMakeItemTag() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(adapter.makeItemTag(android.R.id.list, i), is("android:pager:" + CONTAINER_ID + ":" + i));
		}
		adapter.setMakeItemTags(false);
		assertThat(adapter.makeItemTag(android.R.id.list, 10), is(nullValue()));
	}

	@Test public void testGetItemId() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			assertThat(adapter.getItemId(i), is((long) i));
		}
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testSetPrimaryItem() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment1 = mock(Fragment.class);
		when(mockFragment1.getFragmentManager()).thenReturn(mockFragmentManager);
		adapter.setPrimaryItem(container, 0, mockFragment1);
		assertThat(adapter.getPrimaryFragment(), is(mockFragment1));
		assertThat(adapter.getPrimaryPosition(), is(0));
		verify(mockFragment1, times(1)).setMenuVisibility(true);
		verify(mockFragment1, times(1)).setUserVisibleHint(true);
		final Fragment mockFragment2 = mock(Fragment.class);
		when(mockFragment2.getFragmentManager()).thenReturn(mockFragmentManager);
		// Act:
		adapter.setPrimaryItem(container, 1, mockFragment2);
		// Assert:
		assertThat(adapter.getPrimaryFragment(), is(mockFragment2));
		assertThat(adapter.getPrimaryPosition(), is(1));
		verify(mockFragment2).setMenuVisibility(true);
		verify(mockFragment2).setUserVisibleHint(true);
		verify(mockFragment1).setMenuVisibility(false);
		verify(mockFragment1).setUserVisibleHint(false);
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testSetPrimaryItemThatIsSame() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(mockFragmentManager);
		adapter.setPrimaryItem(container, 0, mockFragment);
		// Act:
		adapter.setPrimaryItem(container, 0, mockFragment);
		// Assert:
		assertThat(adapter.getPrimaryFragment(), is(mockFragment));
		assertThat(adapter.getPrimaryPosition(), is(0));
		verify(mockFragment).setMenuVisibility(true);
		verify(mockFragment).setUserVisibleHint(true);
		verify(mockFragment, times(0)).setMenuVisibility(false);
		verify(mockFragment, times(0)).setUserVisibleHint(false);
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testSetPrimaryItemThatIsNull() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(mockFragmentManager);
		adapter.setPrimaryItem(container, 0, mockFragment);
		// Act:
		adapter.setPrimaryItem(container, 0, null);
		// Assert:
		assertThat(adapter.getPrimaryFragment(), is(nullValue()));
		assertThat(adapter.getPrimaryPosition(), is(0));
		verify(mockFragment).setMenuVisibility(false);
		verify(mockFragment).setUserVisibleHint(false);
	}

	@Test public void testGetPrimaryItemDefault() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		assertThat(adapter.getPrimaryFragment(), is(nullValue()));
	}

	@Test public void testGetPrimaryPositionDefault() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		assertThat(adapter.getPrimaryPosition(), is(FragmentPagerAdapter.NO_POSITION));
	}

	@Test public void testIsViewFromObject() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment = mock(Fragment.class);
		final View view = new View(context());
		when(mockFragment.getView()).thenReturn(view);
		// Act + Assert:
		assertThat(adapter.isViewFromObject(view, mockFragment), is(true));
		when(mockFragment.getView()).thenReturn(new View(context()));
		assertThat(adapter.isViewFromObject(view, mockFragment), is(false));
	}

	@Test public void testDestroyItem() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment = mock(Fragment.class);
		// Act:
		adapter.destroyItem(container, 0, mockFragment);
		// Assert:
		verify(mockFragmentManager).beginTransaction();
		verify(mockTransaction).detach(mockFragment);
		verifyNoMoreInteractions(mockTransaction);
	}

	@Test public void testDestroyItemMultiple() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment1 = mock(Fragment.class);
		final Fragment mockFragment2 = mock(Fragment.class);
		// Act:
		adapter.destroyItem(container, 0, mockFragment1);
		adapter.destroyItem(container, 1, mockFragment2);
		// Assert:
		verify(mockFragmentManager).beginTransaction();
		verify(mockTransaction).detach(mockFragment1);
		verify(mockTransaction).detach(mockFragment2);
		verifyNoMoreInteractions(mockTransaction);
	}

	@Test public void testDestroyItemAtPrimaryPosition() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment = mock(Fragment.class);
		adapter.setPrimaryItem(container, 1, mockFragment);
		// Act:
		adapter.destroyItem(container, 1, mockFragment);
		// Assert:
		assertThat(adapter.getPrimaryFragment(), is(nullValue()));
		assertThat(adapter.getPrimaryPosition(), is(FragmentPagerAdapter.NO_POSITION));
		verify(mockFragmentManager).beginTransaction();
		verify(mockTransaction).detach(mockFragment);
		verifyNoMoreInteractions(mockTransaction);
	}

	@Test public void testFinishUpdate() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(mockFragmentManager);
		adapter.destroyItem(container, 0, mockFragment);
		// Act:
		adapter.finishUpdate(container);
		// Assert:
		verify(mockTransaction).commitNowAllowingStateLoss();
	}

	@Test public void testFinishUpdateWithoutPendingTransaction() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentPagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act:
		adapter.finishUpdate(container);
	}

	private static final class Adapter extends FragmentPagerAdapter {

		Adapter(final FragmentManager fragmentManager) {
			super(fragmentManager);
		}

		@Override public int getCount() {
			return 2;
		}

		@Override @NonNull public Fragment getItem(final int position) {
			return mock(Fragment.class);
		}
	}
}
