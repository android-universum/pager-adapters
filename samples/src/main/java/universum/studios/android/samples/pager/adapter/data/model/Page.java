/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.data.model;

import android.database.Cursor;
import android.os.Parcel;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.model.EntityModelCursorWrapper;
import universum.studios.android.database.model.SimpleEntityModel;
import universum.studios.android.samples.pager.adapter.data.entity.PagesEntity;

/**
 * @author Martin Albedinsky
 */
public final class Page extends SimpleEntityModel<Page> {

	public static final Creator<Page> CREATOR = new Creator<Page>() {

		@Override public Page createFromParcel(@NonNull final Parcel source) {
			return new Page(source);
		}

		@Override public Page[] newArray(final int size) {
			return new Page[size];
		}
	};

	public static final Factory<Page> FACTORY = Page::new;

	@Column.Primary
	@Column(PagesEntity.Columns.ID)
	public Long id;

	@Column(PagesEntity.Columns.TITLE)
	public String title;

	@Column(PagesEntity.Columns.CONTENT)
	public String content;

	private Page() {}

	private Page(@NonNull final Parcel source) {
		super(source);
	}

	@SuppressWarnings("StringBufferReplaceableByString")
	@Override public String toString() {
		final StringBuilder builder = new StringBuilder(64);
		builder.append(getClass().getSimpleName());
		builder.append("{id: ");
		builder.append(id);
		return builder.append("}").toString();
	}

	public static final class CursorWrapper extends EntityModelCursorWrapper<Page> {

		private CursorWrapper(@NonNull final Cursor cursor) {
			super(cursor, FACTORY.createModel());
		}

		@NonNull public static CursorWrapper wrap(@NonNull final Cursor cursor) {
			return cursor instanceof CursorWrapper ? (CursorWrapper) cursor : new CursorWrapper(cursor);
		}
	}
}