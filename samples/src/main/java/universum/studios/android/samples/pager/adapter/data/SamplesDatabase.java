/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.data;

import androidx.annotation.NonNull;
import universum.studios.android.database.Database;
import universum.studios.android.database.annotation.DatabaseEntities;
import universum.studios.android.database.annotation.PrimaryDatabase;
import universum.studios.android.samples.pager.adapter.data.entity.PagesEntity;

/**
 * @author Martin Albedinsky
 */
@DatabaseEntities({
		PagesEntity.class
})
@PrimaryDatabase
public final class SamplesDatabase extends Database {

	public static final class Version {

		public static final int V_1 = 1;
		static final int LATEST = V_1;
	}

	SamplesDatabase() {
		super(Version.LATEST, "data.db");
	}

	@NonNull public static SamplesDatabase get() {
		return (SamplesDatabase) SamplesDatabaseProvider.providePrimaryDatabase();
	}
}