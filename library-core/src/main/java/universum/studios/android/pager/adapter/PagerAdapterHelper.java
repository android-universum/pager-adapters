/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.pager.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

/**
 * Helper class used by the Pager Adapters implementations.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
final class PagerAdapterHelper {

	/**
	 */
	private PagerAdapterHelper() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/**
	 * Sets a user visible hint for the given <var>fragment</var>.
	 *
	 * @param fragment Fragment for which to set user visible hint flag.
	 * @param visible  {@code True} to set user visible hint, {@code false} otherwise.
	 *
	 * @see Fragment#setUserVisibleHint(boolean)
	 */
	static void setUserVisibleHint(@NonNull final Fragment fragment, final boolean visible) {
		final FragmentManager manager = fragment.getFragmentManager();
		if (manager != null) fragment.setUserVisibleHint(visible);
	}

	/**
	 * Finishes update for the given fragment <var>transaction</var>.
	 *
	 * @param fragmentManager The fragment manager that was used to being the given <var>transaction</var>.
	 *                        It may be additionally used to execute pending transactions via
	 *                        {@link FragmentManager#executePendingTransactions()}.
	 * @param transaction     The fragment transaction to be committed in order to finish adapter's update.
	 *
	 * @see FragmentTransaction#commitAllowingStateLoss()
	 * @see FragmentTransaction#commitNowAllowingStateLoss()
	 */
	static void finishUpdate(@SuppressWarnings("unused") @NonNull final FragmentManager fragmentManager, @NonNull final FragmentTransaction transaction) {
		transaction.commitNowAllowingStateLoss();
	}
}