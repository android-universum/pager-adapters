Pager-Adapters-Core
===============

This module contains core elements for this library.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Apager-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Apager-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:pager-adapters-core:${DESIRED_VERSION}@aar"
