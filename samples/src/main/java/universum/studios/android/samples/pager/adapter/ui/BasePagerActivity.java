/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import universum.studios.android.samples.pager.adapter.R;
import universum.studios.android.samples.ui.SamplesActivity;

/**
 * @author Martin Albedinsky
 */
public abstract class BasePagerActivity<A extends PagerAdapter> extends SamplesActivity {

	@BindView(R.id.pager) protected ViewPager pager;
	protected A adapter;

	@NonNull protected static Intent createIntent(@NonNull final Context context, @NonNull final Class<? extends BasePagerActivity> classOfActivity) {
		return new Intent(context, classOfActivity);
	}

	@Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
		requestFeature(FEATURE_DEPENDENCIES_INJECTION | FEATURE_TOOLBAR);
		super.onCreate(savedInstanceState);
	}

	@Override public void onContentChanged() {
		super.onContentChanged();
		if (adapter == null) {
			this.adapter = onCreateAdapter(getSupportFragmentManager());
		}
		this.pager.setAdapter(adapter);
	}

	@NonNull protected abstract A onCreateAdapter(@NonNull FragmentManager fragmentManager);
}