/*
 * *************************************************************************************************
 *                        Copyright (C) 2011 The Android Open Source Project
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.pager.adapter;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;

/**
 * A {@link PagerAdapter} implementation that represents each page as a {@link Fragment} that is
 * persistently kept in the fragment manager as long as a user can return to the page.
 * <p>
 * This version of the pager adapter is best for use when there are a handful of typically more static
 * fragments to be paged through, such as a set of tabs. The fragment of each page the user visits
 * will be kept in memory, though its view hierarchy may be destroyed when not visible. This can
 * result in using a significant amount of memory since fragment instances can hold on to an arbitrary
 * amount of state. For larger sets of pages, consider {@link FragmentStatePagerAdapter}.
 * <p>
 * <b>Note</b>, that when using FragmentPagerAdapter, the host ViewPager must have a valid ID set,
 * otherwise this adapter implementation will throw an exception.
 * <p>
 * The inheritance hierarchies only need to implement {@link #getItem(int)} and
 * {@link #getCount()} to become a fully working adapters.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class FragmentPagerAdapter extends PagerAdapter {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "FragmentPagerAdapter";

	/**
	 * Constant used to identify unspecified position.
	 */
	public static final int NO_POSITION = -1;

	/**
	 * Constant used to identify unspecified id.
	 */
	public static final long NO_ID = -1L;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Fragment manager used to manage fragments provided by this pager adapter.
	 */
	private final FragmentManager fragmentManager;

	/**
	 * Boolean flag indicating whether this pager adapter should create tags for its items or not.
	 *
	 * @see #makeItemTag(int, long)
	 */
	private boolean makeItemTags = true;

	/**
	 * Current pending fragment transaction to be committed in {@link #finishUpdate(ViewGroup)}.
	 */
	private FragmentTransaction pendingTransaction;

	/**
	 * Fragment that is currently the primary item in the associated pager.
	 */
	private Fragment primaryItem;

	/**
	 * Position of the current primary item.
	 */
	private int primaryPosition = NO_POSITION;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of FragmentPagerAdapter with the specified <var>fragmentManager</var>.
	 *
	 * @param fragmentManager The manager used to manage fragments provided by the pager adapter.
	 */
	public FragmentPagerAdapter(@NonNull final FragmentManager fragmentManager) {
		super();
		this.fragmentManager = fragmentManager;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void startUpdate(@NonNull final ViewGroup container) {
		if (container.getId() == View.NO_ID) throw new IllegalStateException(
				"ViewPager with adapter " + this + " requires a view id!"
		);
	}

	/**
	 */
	@SuppressLint("CommitTransaction")
	@Override @NonNull public Object instantiateItem(@NonNull final ViewGroup container, final int position) {
		if (pendingTransaction == null) {
			// Will be committed in call to finishUpdate(...).
			this.pendingTransaction = fragmentManager.beginTransaction();
		}
		final int containerId = container.getId();
		final long itemId = getItemId(position);
		// Check whether we already have this fragment instantiated.
		final String tag = makeItemTag(containerId, itemId);
		Fragment fragment = fragmentManager.findFragmentByTag(tag);
		if (fragment == null) {
			fragment = getItem(position);
			PagerAdaptersLogging.d(TAG, "Adding item(position: " + position + ", id: " + itemId + ", fragment: " + fragment + ").");
			this.pendingTransaction.add(containerId, fragment, tag);
		} else {
			PagerAdaptersLogging.d(TAG, "Attaching item(position: " + position + ", id: " + itemId + ", fragment: " + fragment + ").");
			this.pendingTransaction.attach(fragment);
		}
		if (!fragment.equals(primaryItem)) {
			fragment.setMenuVisibility(false);
			PagerAdapterHelper.setUserVisibleHint(fragment, false);
		}
		return fragment;
	}

	/**
	 * Sets a boolean flag indicating whether this pager adapter should make tags for its items or
	 * not.
	 * <p>
	 * This feature is by default <b>enabled</b>.
	 *
	 * @param makeItemTags {@code True} to enable making of item tags, {@code false} otherwise.
	 *
	 * @see #makeItemTag(int, long)
	 */
	protected final void setMakeItemTags(final boolean makeItemTags) {
		this.makeItemTags = makeItemTags;
	}

	/**
	 * Makes a tag for an item with the given <var>itemId</var>.
	 * <p>
	 * By default making of item tags is <b>enabled</b> and the default implementation makes tag
	 * in the following format:
	 * <pre>
	 * "android:pager:CONTAINER_ID:ITEM_ID"
	 * </pre>
	 * This feature may be enabled/disabled via {@link #setMakeItemTags(boolean)}.
	 *
	 * @param containerId Id of the associated view pager container.
	 * @param itemId      Id of the item for which to create its corresponding tag.
	 * @return Item's tag or {@code null} if this adapter does not create tags for its items.
	 */
	@Nullable protected String makeItemTag(@IdRes final int containerId, final long itemId) {
		return makeItemTags ? "android:pager:" + containerId + ":" + itemId : null;
	}

	/**
	 * Returns a unique identifier for the item at the specified <var>position</var>.
	 * <p>
	 * The default implementation returns the given position. The inheritance hierarchies should
	 * override this method if they have better way to uniquely identify their items.
	 * <p>
	 * <b>Note that this method may be called also from {@link #destroyItem(ViewGroup, int, Object)}
	 * when, possibly, the data set of this adapter may be already invalid. In such case it is safe
	 * to return {@link #NO_ID}.</b>
	 *
	 * @param position Position from the range of size of items of this adapter.
	 * @return Unique identifier for the item at the requested position.
	 */
	public long getItemId(final int position) {
		return position;
	}

	/**
	 * Instantiates a new {@link Fragment} associated with the specified <var>position</var>.
	 *
	 * @see #instantiateItem(ViewGroup, int)
	 */
	@NonNull public abstract Fragment getItem(int position);

	/**
	 */
	@Override public void setPrimaryItem(@NonNull final ViewGroup container, final int position, @Nullable final Object object) {
		final Fragment fragment = (Fragment) object;
		if (primaryItem != fragment) {
			this.primaryPosition = position;
			if (primaryItem != null) {
				this.primaryItem.setMenuVisibility(false);
				PagerAdapterHelper.setUserVisibleHint(primaryItem, false);
			}
			if (fragment != null) {
				fragment.setMenuVisibility(true);
				PagerAdapterHelper.setUserVisibleHint(fragment, true);
			}
			this.primaryItem = fragment;
		}
	}

	/**
	 * Returns the fragment instance that has been set as primary item via {@link #setPrimaryItem(ViewGroup, int, Object)}.
	 *
	 * @return Currently primary fragment. May be {@code null} if no primary item has been set yet.
	 *
	 * @see #getPrimaryPosition()
	 */
	@Nullable public Fragment getPrimaryFragment() {
		return primaryItem;
	}

	/**
	 * Returns position of the current primary item.
	 *
	 * @return Primary item's position or {@link #NO_POSITION} if no primary item has been specified yet.
	 *
	 * @see #getPrimaryFragment()
	 * @see #setPrimaryItem(ViewGroup, int, Object)
	 */
	@IntRange(from = NO_POSITION) public int getPrimaryPosition() {
		return primaryPosition;
	}

	/**
	 */
	@Override public boolean isViewFromObject(@NonNull final View view, @NonNull final Object object) {
		return ((Fragment) object).getView() == view;
	}

	/**
	 */
	@SuppressLint("CommitTransaction")
	@Override public void destroyItem(@NonNull final ViewGroup container, final int position, @NonNull final Object object) {
		if (pendingTransaction == null) {
			// Will be committed in call to finishUpdate(...).
			this.pendingTransaction = fragmentManager.beginTransaction();
		}
		if (position == primaryPosition) {
			this.primaryPosition = NO_POSITION;
			this.primaryItem = null;
		}
		PagerAdaptersLogging.d(TAG, "Detaching item(position: " + position + ", id: " + getItemId(position) + ", fragment: " + object + ", view: " + ((Fragment) object).getView() + ").");
		this.pendingTransaction.detach((Fragment) object);
	}

	/**
	 */
	@Override public void finishUpdate(@NonNull final ViewGroup container) {
		if (pendingTransaction != null) {
			PagerAdapterHelper.finishUpdate(fragmentManager, pendingTransaction);
			this.pendingTransaction = null;
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}