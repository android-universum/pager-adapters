Pager-Adapters-Persistent
===============

This module contains `PagerAdapter` implementation that **persistently keeps** its `Fragment` items 
in `FragmentManager`.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Apager-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Apager-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:pager-adapters-persistent:${DESIRED_VERSION}@aar"

_depends on:_
[pager-adapters-core](https://bitbucket.org/android-universum/pager-adapters/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [FragmentPagerAdapter](https://bitbucket.org/android-universum/pager-adapters/src/main/library-persistent/src/main/java/universum/studios/android/pager/adapter/FragmentPagerAdapter.java)