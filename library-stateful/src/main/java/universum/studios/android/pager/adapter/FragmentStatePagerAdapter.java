/*
 * *************************************************************************************************
 *                        Copyright (C) 2011 The Android Open Source Project
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.pager.adapter;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.collection.LongSparseArray;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;

/**
 * A {@link PagerAdapter} implementation that represents each page as a {@link Fragment}. This pager
 * adapter also handles saving and restoring state of its fragments.
 * <p>
 * This version of the pager adapter is more useful when there are a large number of pages, working
 * more like a list view. When pages are not visible to a user, their entire fragment may be destroyed,
 * only keeping the saved state of that fragment. This allows to the pager to hold on to much less
 * memory associated with each visited page as compared to {@link FragmentPagerAdapter} at the cost
 * of potentially more overhead when switching between pages.
 * <p>
 * <b>Note</b>, that when using FragmentStatePagerAdapter, the host ViewPager must have a valid ID
 * set, otherwise this adapter implementation will throw an exception.
 * <p>
 * The inheritance hierarchies only need to implement {@link #getItem(int)} and {@link #getCount()}
 * to become a fully working adapters.
 *
 * <h3>Not persistent data set</h3>
 * If this adapter is used with <b>not-persistent</b> data set like {@link Cursor} it is essential
 * to override also {@link #getItemId(int)} and provide item id for the requested position as this
 * adapter tracks and stores its created fragments by theirs associated item ids. Item ids are also
 * used to store states of the adapter's fragments. Also note, that {@link #getItemPosition(Object)}
 * method should be implemented in a way which can properly resolve whether a fragment should be
 * still visible at its current position, moved to a different position or to be completely removed
 * due to changes in the data set.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressLint("LongLogTag")
public abstract class FragmentStatePagerAdapter extends PagerAdapter {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "FragmentStatePagerAdapter";

	/**
	 * Constant used to identify unspecified position.
	 */
	public static final int NO_POSITION = -1;

	/**
	 * Constant used to identify unspecified id.
	 */
	public static final long NO_ID = -1L;

	/**
	 * Bundle key used to store states of fragment instances created by this adapter.
	 *
	 * @see #saveState()
	 * @see #restoreState(Parcelable, ClassLoader)
	 */
	@VisibleForTesting static final String STATE_FRAGMENT_STATES = FragmentStatePagerAdapter.class.getName() + ".STATE.FragmentStates";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Fragment manager used to manage fragments provided by this pager adapter.
	 */
	private final FragmentManager fragmentManager;

	/**
	 * Boolean flag indicating whether this pager adapter should create tags for its items or not.
	 *
	 * @see #makeItemTag(int, long)
	 */
	private boolean makeItemTags = true;

	/**
	 * Current pending fragment transaction to be committed in {@link #finishUpdate(ViewGroup)}.
	 */
	private FragmentTransaction pendingTransaction;

	/**
	 * List used to store by this adapter instantiated fragments.
	 */
	private final LongSparseArray<Fragment> fragments = new LongSparseArray<>(5);

	/**
	 * List used to store saved state for by this adapter instantiated fragments.
	 */
	private final LongSparseArray<Fragment.SavedState> savedStates = new LongSparseArray<>(5);

	/**
	 * Fragment that is currently the primary item in the associated pager.
	 */
	private Fragment primaryItem;

	/**
	 * Position of the current primary item.
	 */
	private int primaryPosition = NO_POSITION;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of FragmentStatePagerAdapter with the specified <var>fragmentManager</var>.
	 *
	 * @param fragmentManager The manager used to manage fragments provided by the pager adapter.
	 */
	public FragmentStatePagerAdapter(@NonNull final FragmentManager fragmentManager) {
		super();
		this.fragmentManager = fragmentManager;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void startUpdate(@NonNull final ViewGroup container) {
		if (container.getId() == View.NO_ID) throw new IllegalStateException(
				"ViewPager with adapter " + this + " requires a view id!"
		);
	}

	/**
	 */
	@SuppressLint("CommitTransaction")
	@Override @NonNull public Object instantiateItem(@NonNull final ViewGroup container, final int position) {
		final long itemId = getItemId(position);
		Fragment fragment = fragments.get(itemId);
		// If we already have this item instantiated, there is nothing to do. This can happen when
		// we are restoring the entire pager from its saved state, where the fragment manager has
		// already taken care of restoring the fragments we previously had instantiated.
		if (fragment != null) {
			return fragment;
		}
		if (pendingTransaction == null) {
			// Will be committed in call to finishUpdate(...).
			this.pendingTransaction = fragmentManager.beginTransaction();
		}
		fragment = getItem(position);
		final Fragment.SavedState savedState = savedStates.get(itemId);
		if (savedState != null) {
			fragment.setInitialSavedState(savedState);
		}
		fragment.setMenuVisibility(false);
		PagerAdapterHelper.setUserVisibleHint(fragment, false);
		this.fragments.put(itemId, fragment);
		PagerAdaptersLogging.d(TAG, "Adding item(id: " + itemId + ", fragment: " + fragment + ").");
		final int containerId = container.getId();
		this.pendingTransaction.add(containerId, fragment, makeItemTag(containerId, itemId));
		return fragment;
	}

	/**
	 * Sets a boolean flag indicating whether this pager adapter should make tags for its items or
	 * not.
	 * <p>
	 * This feature is by default <b>enabled</b>.
	 *
	 * @param makeItemTags {@code True} to enable making of item tags, {@code false} otherwise.
	 *
	 * @see #makeItemTag(int, long)
	 */
	protected final void setMakeItemTags(final boolean makeItemTags) {
		this.makeItemTags = makeItemTags;
	}

	/**
	 * Makes a tag for an item with the given <var>itemId</var>.
	 * <p>
	 * By default making of item tags is <b>disabled</b> and the default implementation makes tag
	 * in the following format:
	 * <pre>
	 * "android:pager:CONTAINER_ID:ITEM_ID"
	 * </pre>
	 * This feature may be enabled/disabled via {@link #setMakeItemTags(boolean)}.
	 *
	 * @param containerId Id of the associated view pager container.
	 * @param itemId      Id of the item for which to create its corresponding tag.
	 * @return Item's tag or {@code null} if this adapter does not create tags for its items.
	 */
	@Nullable protected String makeItemTag(@IdRes final int containerId, final long itemId) {
		return makeItemTags ? "android:pager:" + containerId + ":" + itemId : null;
	}

	/**
	 * Returns a unique identifier for the item at the specified <var>position</var>.
	 * <p>
	 * The default implementation returns the given position. The inheritance hierarchies should
	 * override this method if they have better way to uniquely identify their items.
	 * <p>
	 * <b>Note that this method may be called also from {@link #destroyItem(ViewGroup, int, Object)}
	 * when, possibly, the data set of this adapter may be already invalid. In such case it is safe
	 * to return {@link #NO_ID}.</b>
	 *
	 * @param position Position from the range of size of items of this adapter.
	 * @return Unique identifier for the item at the requested position.
	 */
	public long getItemId(final int position) {
		return position;
	}

	/**
	 * Instantiates a new {@link Fragment} associated with the specified <var>position</var>.
	 *
	 * @see #instantiateItem(ViewGroup, int)
	 */
	@NonNull public abstract Fragment getItem(int position);

	/**
	 */
	@Override public void setPrimaryItem(@NonNull final ViewGroup container, final int position, @Nullable final Object object) {
		final Fragment fragment = (Fragment) object;
		if (primaryItem != fragment) {
			this.primaryPosition = position;
			if (primaryItem != null) {
				this.primaryItem.setMenuVisibility(false);
				PagerAdapterHelper.setUserVisibleHint(primaryItem, false);
			}
			if (fragment != null) {
				fragment.setMenuVisibility(true);
				PagerAdapterHelper.setUserVisibleHint(fragment, true);
			}
			this.primaryItem = fragment;
		}
	}

	/**
	 * Returns the fragment instance that has been set as primary item via {@link #setPrimaryItem(ViewGroup, int, Object)}.
	 *
	 * @return Currently primary fragment. May be {@code null} if no primary item has been set yet.
	 *
	 * @see #getPrimaryPosition()
	 */
	@Nullable public Fragment getPrimaryFragment() {
		return primaryItem;
	}

	/**
	 * Returns position of the current primary item.
	 *
	 * @return Primary item's position or {@link #NO_POSITION} if no primary item has been specified yet.
	 *
	 * @see #getPrimaryFragment()
	 * @see #setPrimaryItem(ViewGroup, int, Object)
	 */
	@IntRange(from = NO_POSITION) public int getPrimaryPosition() {
		return primaryPosition;
	}

	/**
	 */
	@Override public boolean isViewFromObject(@NonNull final View view, @NonNull final Object object) {
		return ((Fragment) object).getView() == view;
	}

	/**
	 */
	@SuppressLint("CommitTransaction")
	@Override public void destroyItem(@NonNull final ViewGroup container, final int position, @NonNull final Object object) {
		if (pendingTransaction == null) {
			// Will be committed in call to finishUpdate(...).
			this.pendingTransaction = fragmentManager.beginTransaction();
		}
		if (position == primaryPosition) {
			this.primaryPosition = NO_POSITION;
			this.primaryItem = null;
		}
		final Fragment fragment = (Fragment) object;
		final int itemIndex = fragments.indexOfValue(fragment);
		if (itemIndex >= 0) {
			final long itemId = fragments.keyAt(itemIndex);
			PagerAdaptersLogging.d(TAG, "Removing item(id: " + itemId + ", fragment: " + fragment + ", view: " + fragment.getView() + ").");
			if (fragment.isAdded()) {
				this.savedStates.put(itemId, fragmentManager.saveFragmentInstanceState(fragment));
			} else {
				this.savedStates.remove(itemId);
			}
			this.fragments.remove(itemId);
		}
		this.pendingTransaction.remove(fragment);
	}

	/**
	 */
	@Override public void finishUpdate(@NonNull final ViewGroup container) {
		if (pendingTransaction != null) {
			PagerAdapterHelper.finishUpdate(fragmentManager, pendingTransaction);
			this.pendingTransaction = null;
		}
	}

	/**
	 */
	@Override public Parcelable saveState() {
		Bundle state = null;
		final int statesCount = savedStates.size();
		if (statesCount > 0) {
			state = new Bundle();
			final long[] savedStateIds = new long[statesCount];
			for (int i = 0; i < statesCount; i++) {
				final long itemId = savedStates.keyAt(i);
				savedStateIds[i] = itemId;
				state.putParcelable("android:pager:fragment_state:" + itemId, savedStates.valueAt(i));
			}
			state.putLongArray(STATE_FRAGMENT_STATES, savedStateIds);
		}
		for (int i = 0; i < fragments.size(); i++) {
			final long itemId = fragments.keyAt(i);
			final Fragment fragment = fragments.get(itemId);
			if (fragment.isAdded()) {
				if (state == null) {
					state = new Bundle();
				}
				this.fragmentManager.putFragment(state, "android:pager:fragment:" + itemId, fragment);
			}
		}
		return state;
	}

	/**
	 */
	@Override public void restoreState(@Nullable final Parcelable state, @Nullable final ClassLoader loader) {
		if (state instanceof Bundle) {
			final Bundle bundle = (Bundle) state;
			bundle.setClassLoader(loader);
			final long[] savedStateIds = bundle.getLongArray(STATE_FRAGMENT_STATES);
			this.savedStates.clear();
			this.fragments.clear();
			if (savedStateIds != null && savedStateIds.length > 0) {
				for (final long itemId : savedStateIds) {
					this.savedStates.put(itemId, bundle.<Fragment.SavedState>getParcelable("android:pager:fragment_state:" + itemId));
				}
			}
			final Iterable<String> keys = bundle.keySet();
			for (final String key : keys) {
				if (key.startsWith("android:pager:fragment:")) {
					final Fragment fragment = fragmentManager.getFragment(bundle, key);
					if (fragment == null) {
						PagerAdaptersLogging.w(TAG, "Fragment not found for the key " + key + " when restoring adapter's state.");
					} else {
						fragment.setMenuVisibility(false);
						this.fragments.put(Long.parseLong(key.split(":")[3]), fragment);
					}
				}
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}