/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.OnClick;
import universum.studios.android.samples.pager.adapter.R;
import universum.studios.android.samples.pager.adapter.data.model.Page;
import universum.studios.android.samples.pager.adapter.databinding.FragmentPageBinding;
import universum.studios.android.samples.ui.SamplesFragment;

/**
 * @author Martin Albedinsky
 */
public final class PageFragment extends SamplesFragment implements CompoundButton.OnCheckedChangeListener {

	@SuppressWarnings("unused") private static final String TAG = "PageFragment";
	private static final String ARGUMENT_PAGE = PageFragment.class.getName() + ".ARGUMENT.Page";
	private static final String STATE_CHECKED = PageFragment.class.getName() + ".STATE.Checked";

	private Page page;
	private boolean checked;
	private FragmentPageBinding binding;

	@NonNull public static PageFragment newInstance(@NonNull final Page page) {
		final PageFragment fragment = new PageFragment();
		final Bundle args = new Bundle();
		args.putParcelable(ARGUMENT_PAGE, page);
		fragment.setArguments(args);
		return fragment;
	}

	public long getPageId() {
		return page != null ? page.id : -1;
	}

	@SuppressWarnings("ConstantConditions")
	@Override public void onCreate(@Nullable final Bundle savedInstanceState) {
		requestFeature(FEATURE_DEPENDENCIES_INJECTION);
		super.onCreate(savedInstanceState);
		this.page = ((Page) getArguments().getParcelable(ARGUMENT_PAGE)).clone();
	}

	@Override @Nullable public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_page, container, false);
	}

	@Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setHasOptionsMenu(true);
		if (savedInstanceState != null) {
			this.checked = savedInstanceState.getBoolean(STATE_CHECKED, false);
		}
		Log.d(TAG, "onViewCreated() page(id: " + page.id + ") checked = " + checked);
		this.binding = FragmentPageBinding.bind(view);
		this.binding.setPage(page);
		this.binding.setChecked(checked);
		this.binding.setOnCheckedChangeListener(this);
		this.binding.executePendingBindings();
	}

	@Override public void onCheckedChanged(@NonNull final CompoundButton buttonView, final boolean isChecked) {
		this.checked = isChecked;
	}

	@OnClick({
			R.id.page_title_frame
	})
	@SuppressWarnings("unused")
	void onViewClick(@NonNull final View view) {
		switch (view.getId()) {
			case R.id.page_title_frame:
				setChecked(!isChecked());
				break;
		}
	}

	void setChecked(final boolean checked) {
		if (this.checked != checked) {
			this.checked = checked;
			if (binding != null) {
				binding.setChecked(checked);
			}
		}
	}

	boolean isChecked() {
		return checked;
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		setHasOptionsMenu(false);
		Log.d(TAG, "onDestroyView() page(id: " + page.id + ") checked = " + checked);
		this.binding = null;
	}

	@Override public void onSaveInstanceState(@NonNull final Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(TAG, "onSaveInstanceState(...) page(id: " + page.id + ") checked = " + checked);
		outState.putBoolean(STATE_CHECKED, checked);
	}
}