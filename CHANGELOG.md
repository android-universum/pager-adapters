Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 2.x ##

### [2.2.2](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 28.04.2020

- Regular **maintenance**.

## Version 2.x ##

### [2.2.1](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 07.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [2.2.0](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 16.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [2.1.4](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 20.06.2018

- Small updates.

### [2.1.3](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 10.12.2017

- Method `instantiateItem(...)` annotated with `@NonNull` annotation for both, _persistent_ and
  _stateful_ adapter implementations.

### [2.1.2](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 22.10.2017

- Stability improvements.

### [2.1.1](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 28.07.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_ for both versions of the library.

### [2.1.0](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 05.05.2017

- Fixed implementation of `finishUpdate(ViewGroup)` for both `FragmentPagerAdapter` and `FragmentStatePagerAdapter`.
- Also **primary item/fragment** is de-initialized whenever adapter receives `destroyItem(ViewGroup, int, Object)`
  call with current primary position.

### [2.0.3](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 29.04.2017

- Removed **deprecated** `PagerAdaptersConfig` class.

### [2.0.2](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 21.04.2017

- `FragmentStatePagerAdapter` now properly saves fragments (puts them into `FragmentManager`) when `saveState()` 
  is called upon such adapter.
- Deprecated `PagerAdaptersConfig` and added `PagerAdaptersLogging` in order to provide logging control
  for the library.

### [2.0.1](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 01.03.2017

- Primary position is now properly updated for `FragmentPagerAdapter` and also for `FragmentStatePagerAdapter`.

### [2.0.0](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 05.02.2017

- Removed all **deprecated elements** from the previous beta releases.

### [2.0.0-beta1](https://bitbucket.org/android-universum/pager-adapters/wiki/version/2.x) ###
> 21.01.2017

- Moved `getPrimaryFragment()` and `getPrimaryPosition()` from base adapters to core adapters.
- Deprecated `BaseFragmentPagerAdapter` and `BaseFragmentStatePagerAdapter`. As theirs methods
  has been moved to the upper level both these classes lost theirs purpose.
- `FragmentStatePagerAdapter` now stores and tracks its fragments based on item id specified for
  these fragments instead of just theirs positions in the data set.
- `FragmentStatePagerAdapter` has now **item tags** feature **enabled** by default.

## [Version 1.x](https://bitbucket.org/android-universum/pager-adapters/wiki/version/1.x) ##