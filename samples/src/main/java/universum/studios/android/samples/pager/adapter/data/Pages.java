/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.data;

import androidx.annotation.NonNull;
import universum.studios.android.database.loremipsum.LoremIpsum;
import universum.studios.android.samples.pager.adapter.data.model.Page;

/**
 * @author Martin Albedinsky
 */
public final class Pages {

	@NonNull public static Page random() {
		final Page page = Page.FACTORY.createModel();
		page.title = LoremIpsum.generateTitle();
		page.content = LoremIpsum.generateContent();
		return page;
	}
}