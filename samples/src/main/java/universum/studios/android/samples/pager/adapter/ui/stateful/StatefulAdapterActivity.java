/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.ui.stateful;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import butterknife.BindView;
import butterknife.OnClick;
import universum.studios.android.database.adapter.OnCursorDataSetListener;
import universum.studios.android.samples.pager.adapter.R;
import universum.studios.android.samples.pager.adapter.data.Pages;
import universum.studios.android.samples.pager.adapter.data.transaction.DeletePageWithIdTransaction;
import universum.studios.android.samples.pager.adapter.data.transaction.InsertNewPageTransaction;
import universum.studios.android.samples.pager.adapter.ui.BasePagerActivity;

/**
 * @author Martin Albedinsky
 */
public final class StatefulAdapterActivity extends BasePagerActivity<SampleAdapter> implements OnCursorDataSetListener<SampleAdapter> {

	@BindView(android.R.id.empty) View emptyView;
	private boolean newPageLoadPending;

	public static void start(@NonNull final Activity caller) {
		caller.startActivity(createIntent(caller, StatefulAdapterActivity.class));
	}

	@Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pager_state);
	}

	@Override public void onContentChanged() {
		super.onContentChanged();
		this.pager.setOffscreenPageLimit(1);
	}

	@Override @NonNull protected SampleAdapter onCreateAdapter(@NonNull final FragmentManager fragmentManager) {
		final SampleAdapter adapter = new SampleAdapter(this);
		adapter.registerOnCursorDataSetListener(this);
		adapter.initLoader();
		return adapter;
	}

	@Override public boolean onCreateOptionsMenu(@NonNull final Menu menu) {
		getMenuInflater().inflate(R.menu.pages, menu);
		menu.findItem(R.id.pages_item_delete).setIcon(VectorDrawableCompat.create(
				getResources(),
				R.drawable.vc_delete_24dp,
				getTheme()
		));
		return true;
	}

	@Override public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
		switch (item.getItemId()) {
			case R.id.pages_item_delete:
				new DeletePageWithIdTransaction(adapter.getItemId(pager.getCurrentItem())).executeAsync();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override public void onCursorDataSetLoaded(@NonNull final SampleAdapter adapter, final int loaderId) {
		this.emptyView.setVisibility(adapter.isEmpty() ? View.VISIBLE : View.INVISIBLE);
		if (newPageLoadPending) {
			this.newPageLoadPending = false;
			this.pager.setCurrentItem(adapter.getItemCount() - 1, true);
		}
	}

	@Override public void onCursorDataSetChanged(@NonNull SampleAdapter adapter) {}

	@Override public void onCursorDataSetInvalidated(@NonNull SampleAdapter adapter) {}

	@OnClick(R.id.fab)
	@SuppressWarnings("unused")
	void onViewClick(@NonNull final View view) {
		this.newPageLoadPending = true;
		new InsertNewPageTransaction(Pages.random()).executeAsync();
	}
}