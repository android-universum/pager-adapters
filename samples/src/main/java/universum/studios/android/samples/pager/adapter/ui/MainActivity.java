/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.ui;

import android.view.MenuItem;

import androidx.annotation.NonNull;
import universum.studios.android.samples.pager.adapter.R;
import universum.studios.android.samples.pager.adapter.ui.persistent.PersistentAdapterActivity;
import universum.studios.android.samples.pager.adapter.ui.stateful.StatefulAdapterActivity;
import universum.studios.android.samples.ui.SamplesNavigationActivity;

/**
 * @author Martin Albedinsky
 */
public final class MainActivity extends SamplesNavigationActivity {

	@Override protected boolean onHandleNavigationItemSelected(@NonNull final MenuItem item) {
		switch (item.getItemId()) {
			case R.id.navigation_item_adapter_simple:
				closeNavigation();
				PersistentAdapterActivity.start(this);
				return false;
			case R.id.navigation_item_adapter_stateful:
				closeNavigation();
				StatefulAdapterActivity.start(this);
				return false;
			default:
				return true;
		}
	}
}