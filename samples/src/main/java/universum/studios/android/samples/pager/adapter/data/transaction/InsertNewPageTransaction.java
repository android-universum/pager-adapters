/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.data.transaction;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseTransaction;
import universum.studios.android.samples.pager.adapter.data.SamplesDatabase;
import universum.studios.android.samples.pager.adapter.data.entity.PagesEntity;
import universum.studios.android.samples.pager.adapter.data.model.Page;

/**
 * @author Martin Albedinsky
 */
public final class InsertNewPageTransaction extends DatabaseTransaction<Void, SamplesDatabase> {

	private final Page page;

	public InsertNewPageTransaction(@NonNull final Page page) {
		super(SamplesDatabase.class);
		this.page = page;
	}

	@Override @Nullable protected Void onExecute(@NonNull final SamplesDatabase database) {
		database.findEntityByClass(PagesEntity.class).insertModel(page);
		return null;
	}
}