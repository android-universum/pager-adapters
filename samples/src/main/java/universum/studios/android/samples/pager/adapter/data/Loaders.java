/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.data;

import android.content.Context;
import android.database.Cursor;

import androidx.annotation.NonNull;
import androidx.loader.content.Loader;
import universum.studios.android.database.content.CursorLoaderBuilder;
import universum.studios.android.database.content.CursorLoaderFactory;
import universum.studios.android.samples.pager.adapter.data.entity.PagesEntity;

/**
 * @author Martin Albedinsky
 */
public final class Loaders extends CursorLoaderFactory {

	public static final int PAGES = 1;

	@NonNull public static Loader<Cursor> createPagesLoader(@NonNull final Context context) {
		return new CursorLoaderBuilder(PagesEntity.get().getContentUri()).build(context);
	}
}