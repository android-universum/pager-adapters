/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.pager.adapter.data.entity;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.EntityName;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.entity.ModelEntity;
import universum.studios.android.samples.pager.adapter.data.SamplesDatabase;
import universum.studios.android.samples.pager.adapter.data.model.Page;

/**
 * @author Martin Albedinsky
 */
@Model(Page.class)
@EntityName(PagesEntity.NAME)
public final class PagesEntity extends ModelEntity<Page> {

	public interface Columns {

		String ID = Column.Primary.COLUMN_NAME;
		String TITLE = "title";
		String CONTENT = "content";
	}

	public static final String NAME = "pages";

	@NonNull public static PagesEntity get() {
		return SamplesDatabase.get().findEntityByClass(PagesEntity.class);
	}
}