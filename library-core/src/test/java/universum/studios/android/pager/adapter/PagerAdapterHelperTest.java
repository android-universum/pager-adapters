/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.pager.adapter;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import universum.studios.android.test.AndroidTestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
public final class PagerAdapterHelperTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		PagerAdapterHelper.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<PagerAdapterHelper> constructor = PagerAdapterHelper.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testSetUserVisibleHint() {
		// Arrange:
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(mock(FragmentManager.class));
		// Act:
		PagerAdapterHelper.setUserVisibleHint(mockFragment, true);
		PagerAdapterHelper.setUserVisibleHint(mockFragment, false);
		// Assert:
		verify(mockFragment, times(2)).getFragmentManager();
		verify(mockFragment, times(1)).setUserVisibleHint(true);
		verify(mockFragment, times(1)).setUserVisibleHint(false);
	}

	@Test public void testSetUserVisibleHintWithoutAttachedFragmentManager() {
		// Arrange:
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(null);
		// Act:
		PagerAdapterHelper.setUserVisibleHint(mockFragment, true);
		PagerAdapterHelper.setUserVisibleHint(mockFragment, false);
		// Assert:
		verify(mockFragment, times(2)).getFragmentManager();
		verifyNoMoreInteractions(mockFragment);
	}

	@Test public void testFinishUpdate() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		// Act:
		PagerAdapterHelper.finishUpdate(mockFragmentManager, mockTransaction);
		// Assert:
		verify(mockTransaction).commitNowAllowingStateLoss();
		verifyNoInteractions(mockFragmentManager);
	}
}