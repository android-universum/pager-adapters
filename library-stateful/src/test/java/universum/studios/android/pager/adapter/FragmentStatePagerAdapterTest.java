/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.pager.adapter;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.hamcrest.core.Is;
import org.junit.Test;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class FragmentStatePagerAdapterTest extends AndroidTestCase {

	private static final int CONTAINER_ID = android.R.id.list;

	private ViewGroup container;

	@Override public void beforeTest() {
		super.beforeTest();
		this.container = new FrameLayout(context());
		this.container.setId(CONTAINER_ID);
	}

	@Override public void afterTest() {
		super.afterTest();
		this.container = null;
	}

	@Test public void testConstants() {
		// Assert:
		assertThat(FragmentStatePagerAdapter.NO_POSITION, is(-1));
		assertThat(FragmentStatePagerAdapter.NO_ID, is(-1L));
	}

	@Test public void testStartUpdate() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act:
		adapter.startUpdate(container);
	}

	@Test(expected = IllegalStateException.class)
	public void testStartUpdateWithViewWithoutId() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act:
		adapter.startUpdate(new FrameLayout(context()));
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testInstantiateItemBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testInstantiateItemAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareInstantiateItemTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		// Act:
		final Fragment fragment = (Fragment) adapter.instantiateItem(container, 0);
		// Assert:
		assertThat(fragment, is(notNullValue()));
		verify(mockFragmentManager).beginTransaction();
		verify(mockTransaction).add(CONTAINER_ID, fragment, adapter.makeItemTag(CONTAINER_ID, 0));
		return mockTransaction;
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testInstantiateItemMultipleBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemMultipleTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testInstantiateItemMultipleAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemMultipleTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareInstantiateItemMultipleTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		// Act:
		final Fragment fragment1 = (Fragment) adapter.instantiateItem(container, 0);
		final Fragment fragment2 = (Fragment) adapter.instantiateItem(container, 1);
		// Assert:
		assertThat(fragment1, is(notNullValue()));
		assertThat(fragment2, is(notNullValue()));
		assertThat(fragment1, is(not(fragment2)));
		verify(mockFragmentManager).beginTransaction();
		verify(mockTransaction).add(CONTAINER_ID, fragment1, adapter.makeItemTag(CONTAINER_ID, 0));
		verify(mockTransaction).add(CONTAINER_ID, fragment2, adapter.makeItemTag(CONTAINER_ID, 1));
		return mockTransaction;
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testInstantiateItemAlreadyAddedBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemAlreadyAddedTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testInstantiateItemAlreadyAddedAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemAlreadyAddedTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareInstantiateItemAlreadyAddedTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		// Act:
		final Fragment fragment = (Fragment) adapter.instantiateItem(container, 0);
		// Assert:
		assertThat(fragment, is((Fragment) adapter.instantiateItem(container, 0)));
		verify(mockFragmentManager).beginTransaction();
		verify(mockTransaction).add(CONTAINER_ID, fragment, adapter.makeItemTag(CONTAINER_ID, 0));
		return mockTransaction;
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testInstantiateItemWithSavedStateBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemWithSavedStateTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testInstantiateItemWithSavedStateAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareInstantiateItemWithSavedStateTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareInstantiateItemWithSavedStateTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final Adapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment = mock(Fragment.class);
		final Fragment.SavedState mockFragmentState = mock(Fragment.SavedState.class);
		final Bundle savedState = new Bundle();
		savedState.putLongArray(FragmentStatePagerAdapter.STATE_FRAGMENT_STATES, new long[]{adapter.getItemId(0)});
		savedState.putParcelable("android:pager:fragment_state:" + adapter.getItemId(0), mockFragmentState);
		adapter.setFragmentFactory(position -> mockFragment);
		adapter.restoreState(savedState, Adapter.class.getClassLoader());
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		// Act:
		final Fragment fragment = (Fragment) adapter.instantiateItem(container, 0);
		// Assert:
		verify(mockFragmentManager).beginTransaction();
		verify(mockFragment).setInitialSavedState(mockFragmentState);
		verify(mockTransaction).add(CONTAINER_ID, fragment, adapter.makeItemTag(CONTAINER_ID, 0));
		return mockTransaction;
	}

	@Test public void testMakeItemTag() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(adapter.makeItemTag(android.R.id.list, i), is("android:pager:" + CONTAINER_ID + ":" + i));
		}
		adapter.setMakeItemTags(false);
		assertThat(adapter.makeItemTag(android.R.id.list, 10), is(nullValue()));
	}

	@Test public void testGetItemId() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			assertThat(adapter.getItemId(i), is((long) i));
		}
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testSetPrimaryItem() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment1 = mock(Fragment.class);
		when(mockFragment1.getFragmentManager()).thenReturn(mockFragmentManager);
		adapter.setPrimaryItem(container, 0, mockFragment1);
		assertThat(adapter.getPrimaryFragment(), is(mockFragment1));
		assertThat(adapter.getPrimaryPosition(), is(0));
		verify(mockFragment1, times(1)).setMenuVisibility(true);
		verify(mockFragment1, times(1)).setUserVisibleHint(true);
		final Fragment mockFragment2 = mock(Fragment.class);
		when(mockFragment2.getFragmentManager()).thenReturn(mockFragmentManager);
		// Act:
		adapter.setPrimaryItem(container, 1, mockFragment2);
		// Assert:
		assertThat(adapter.getPrimaryFragment(), is(mockFragment2));
		assertThat(adapter.getPrimaryPosition(), is(1));
		verify(mockFragment2).setMenuVisibility(true);
		verify(mockFragment2).setUserVisibleHint(true);
		verify(mockFragment1).setMenuVisibility(false);
		verify(mockFragment1).setUserVisibleHint(false);
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testSetPrimaryItemThatIsSame() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(mockFragmentManager);
		adapter.setPrimaryItem(container, 0, mockFragment);
		// Act:
		adapter.setPrimaryItem(container, 0, mockFragment);
		// Assert:
		assertThat(adapter.getPrimaryFragment(), is(mockFragment));
		assertThat(adapter.getPrimaryPosition(), is(0));
		verify(mockFragment).setMenuVisibility(true);
		verify(mockFragment).setUserVisibleHint(true);
		verify(mockFragment, times(0)).setMenuVisibility(false);
		verify(mockFragment, times(0)).setUserVisibleHint(false);
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testSetPrimaryItemThatIsNull() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(mockFragmentManager);
		adapter.setPrimaryItem(container, 0, mockFragment);
		// Act:
		adapter.setPrimaryItem(container, 0, null);
		// Assert:
		assertThat(adapter.getPrimaryFragment(), is(nullValue()));
		assertThat(adapter.getPrimaryPosition(), is(0));
		verify(mockFragment).setMenuVisibility(false);
		verify(mockFragment).setUserVisibleHint(false);
	}

	@Test public void testGetPrimaryItemDefault() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		assertThat(adapter.getPrimaryFragment(), is(nullValue()));
	}

	@Test public void testGetPrimaryPositionDefault() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		assertThat(adapter.getPrimaryPosition(), is(FragmentStatePagerAdapter.NO_POSITION));
	}

	@Test public void testIsViewFromObject() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final Fragment mockFragment = mock(Fragment.class);
		final View view = new View(context());
		when(mockFragment.getView()).thenReturn(view);
		// Act + Assert:
		assertThat(adapter.isViewFromObject(view, mockFragment), is(true));
		when(mockFragment.getView()).thenReturn(new View(context()));
		assertThat(adapter.isViewFromObject(view, mockFragment), is(false));
	}

	@Test public void testDestroyItem() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment fragment = (Fragment) adapter.instantiateItem(container, 0);
		// Act:
		adapter.destroyItem(container, 0, fragment);
		// Assert:
		verify(mockFragmentManager).beginTransaction();
		verify(mockTransaction).add(anyInt(), any(Fragment.class), anyString());
		verify(mockTransaction).remove(fragment);
		verifyNoMoreInteractions(mockTransaction);
	}

	@Test public void testDestroyItemAlreadyAdded() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Adapter adapter = new Adapter(mockFragmentManager);
		adapter.setFragmentFactory(position -> new TestFragment());
		final Fragment fragment = (Fragment) adapter.instantiateItem(container, 0);
		adapter.finishUpdate(container);
		mockFragmentManager.executePendingTransactions();
		// Act:
		adapter.destroyItem(container, 0, fragment);
		mockFragmentManager.executePendingTransactions();
		// Assert:
		assertThat(mockFragmentManager.findFragmentByTag(adapter.makeItemTag(CONTAINER_ID, 0)), is(nullValue()));
		verify(mockTransaction).add(anyInt(), any(Fragment.class), anyString());
		verify(mockTransaction).remove(fragment);
	}

	@Test public void testDestroyItemMultiple() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment fragment1 = (Fragment) adapter.instantiateItem(container, 0);
		final Fragment fragment2 = (Fragment) adapter.instantiateItem(container, 1);
		// Act:
		adapter.destroyItem(container, 0, fragment1);
		adapter.destroyItem(container, 1, fragment2);
		// Assert:
		verify(mockFragmentManager, times(1)).beginTransaction();
		verify(mockTransaction, times(2)).add(anyInt(), any(Fragment.class), anyString());
		verify(mockTransaction, times(1)).remove(fragment1);
		verify(mockTransaction, times(1)).remove(fragment2);
		verifyNoMoreInteractions(mockTransaction);
	}

	@Test public void testDestroyItemAtPrimaryPosition() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment = mock(Fragment.class);
		adapter.setPrimaryItem(container, 1, mockFragment);
		// Act:
		adapter.destroyItem(container, 1, mockFragment);
		// Assert:
		assertThat(adapter.getPrimaryFragment(), is(nullValue()));
		assertThat(adapter.getPrimaryPosition(), is(FragmentStatePagerAdapter.NO_POSITION));
		verify(mockFragmentManager).beginTransaction();
		verify(mockTransaction).remove(mockFragment);
		verifyNoMoreInteractions(mockTransaction);
	}

	@Test public void testDestroyItemNotInstantiated() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment = mock(Fragment.class);
		// Act:
		adapter.destroyItem(container, 0, mockFragment);
		// Assert:
		verify(mockTransaction).remove(mockFragment);
		verifyNoMoreInteractions(mockTransaction);
	}

	@Test public void testFinishUpdate() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragment.getFragmentManager()).thenReturn(mockFragmentManager);
		adapter.destroyItem(container, 0, mockFragment);
		// Act:
		adapter.finishUpdate(container);
		// Assert:
		verify(mockTransaction).commitNowAllowingStateLoss();
	}

	@Test public void testFinishUpdateWithoutPendingTransaction() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act:
		adapter.finishUpdate(container);
	}

	@Test public void testSaveState() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final Adapter adapter = new Adapter(mockFragmentManager);
		final int mockCount = 5;
		// Initialize adapter's saved states.
		final Bundle restoreState = new Bundle();
		final long[] mockFragmentStateIds = new long[mockCount];
		final Fragment.SavedState[] mockFragmentStates = new Fragment.SavedState[mockCount];
		for (int i = 0; i < mockCount; i++) {
			final long itemId = adapter.getItemId(i);
			mockFragmentStateIds[i] = itemId;
			mockFragmentStates[i] = mock(Fragment.SavedState.class);
			restoreState.putParcelable("android:pager:fragment_state:" + itemId, mockFragmentStates[i]);
		}
		restoreState.putLongArray(FragmentStatePagerAdapter.STATE_FRAGMENT_STATES, mockFragmentStateIds);
		adapter.restoreState(restoreState, Adapter.class.getClassLoader());
		// Instantiate some fragments.
		final FragmentActivity activity = Robolectric.buildActivity(TestActivity.class).create().start().resume().get();
		final ViewGroup activityContentView = activity.findViewById(TestActivity.CONTENT_VIEW_ID);
		final FragmentManager fragmentManager = activity.getSupportFragmentManager();
		adapter.setFragmentFactory(position -> new TestFragment());
		when(mockFragmentManager.beginTransaction()).thenReturn(fragmentManager.beginTransaction());
		final Fragment[] fragments = new Fragment[mockCount];
		for (int i = 0; i < mockCount; i++) {
			fragments[i] = (Fragment) adapter.instantiateItem(activityContentView, i);
		}
		adapter.finishUpdate(activityContentView);
		// Simulate fragment that is not added.
		fragmentManager.beginTransaction().remove(fragments[0]).commitAllowingStateLoss();
		fragmentManager.executePendingTransactions();
		// Act:
		final Parcelable state = adapter.saveState();
		// Assert:
		// Verify that the saved state contains saved states for the instantiated fragments.
		assertThat(state, is(notNullValue()));
		assertThat(state, instanceOf(Bundle.class));
		final Bundle savedState = (Bundle) state;
		final long[] savedStateIds = savedState.getLongArray(FragmentStatePagerAdapter.STATE_FRAGMENT_STATES);
		assertThat(savedStateIds, is(notNullValue()));
		assertThat(savedStateIds.length, is(mockCount));
		for (int i = 0; i < mockCount; i++) {
			final long itemId = adapter.getItemId(i);
			assertThat(savedStateIds[i], is(itemId));
			assertThat(savedState.getParcelable("android:pager:fragment_state:" + itemId), Is.<Parcelable>is(mockFragmentStates[i]));
			if (i == 0) {
				verify(mockFragmentManager, times(0)).putFragment(savedState, "android:pager:fragment:" + itemId, fragments[i]);
			} else {
				verify(mockFragmentManager, times(1)).putFragment(savedState, "android:pager:fragment:" + itemId, fragments[i]);
			}
		}
	}

	@Test public void testSaveStateWithoutSavedStates() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final Adapter adapter = new Adapter(mockFragmentManager);
		adapter.setFragmentFactory(position -> new TestFragment());
		final FragmentActivity activity = Robolectric.buildActivity(TestActivity.class).create().start().resume().get();
		final ViewGroup activityContentView = activity.findViewById(TestActivity.CONTENT_VIEW_ID);
		final FragmentManager fragmentManager = activity.getSupportFragmentManager();
		when(mockFragmentManager.beginTransaction()).thenReturn(fragmentManager.beginTransaction());
		final Fragment fragment = (Fragment) adapter.instantiateItem(activityContentView, 0);
		adapter.finishUpdate(activityContentView);
		fragmentManager.executePendingTransactions();
		// Act:
		final Parcelable state = adapter.saveState();
		// Assert:
		assertThat(state, is(notNullValue()));
		assertThat(state, instanceOf(Bundle.class));
		verify(mockFragmentManager).putFragment((Bundle) state, "android:pager:fragment:" + adapter.getItemId(0), fragment);
	}

	@Test public void testSaveStateWithoutInstantiatedFragments() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		final Bundle restoreState = new Bundle();
		final Fragment.SavedState mockFragmentState = mock(Fragment.SavedState.class);
		final long itemId = adapter.getItemId(0);
		restoreState.putParcelable("android:pager:fragment_state:" + itemId, mockFragmentState);
		restoreState.putLongArray(FragmentStatePagerAdapter.STATE_FRAGMENT_STATES, new long[]{itemId});
		adapter.restoreState(restoreState, Adapter.class.getClassLoader());
		// Act:
		final Parcelable state = adapter.saveState();
		// Assert:
		assertThat(state, is(notNullValue()));
		assertThat(state, instanceOf(Bundle.class));
		final Bundle savedState = (Bundle) state;
		final long[] savedStateIds = savedState.getLongArray(FragmentStatePagerAdapter.STATE_FRAGMENT_STATES);
		assertThat(savedStateIds, is(notNullValue()));
		assertThat(savedStateIds.length, is(1));
		assertThat(savedStateIds[0], is(itemId));
		assertThat(savedState.getParcelable("android:pager:fragment_state:" + itemId), Is.<Parcelable>is(mockFragmentState));
	}

	@Test public void testSaveStateOnEmptyAdapter() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act + Assert:
		assertThat(adapter.saveState(), is(nullValue()));
	}

	@Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
	@Test public void testRestoreStateBelowNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareRestoreStateTest(), times(0)).commitAllowingStateLoss();
	}

	@Config(sdk = Build.VERSION_CODES.N)
	@Test public void testRestoreStateAboveNougatApiLevel() {
		// Assert:
		verify(performApiLevelUnawareRestoreStateTest(), times(0)).commitNowAllowingStateLoss();
	}

	private FragmentTransaction performApiLevelUnawareRestoreStateTest() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final Adapter adapter = new Adapter(mockFragmentManager);
		final int mockCount = 5;
		final Bundle savedState = new Bundle();
		// Fill the saved state with fragment states.
		final Fragment.SavedState[] mockFragmentStates = new Fragment.SavedState[mockCount];
		final long[] mockItemIds = new long[mockCount];
		for (int i = 0; i < mockCount; i++) {
			final long itemId = adapter.getItemId(i);
			mockItemIds[i] = itemId;
			final Fragment.SavedState mockFragmentState = mock(Fragment.SavedState.class);
			mockFragmentStates[i] = mockFragmentState;
			savedState.putParcelable("android:pager:fragment_state:" + itemId, mockFragmentState);
		}
		savedState.putLongArray(FragmentStatePagerAdapter.STATE_FRAGMENT_STATES, mockItemIds);
		final Fragment[] mockFragments = new Fragment[mockCount];
		for (int i = 0; i < mockCount; i++) {
			mockFragments[i] = mock(Fragment.class);
		}
		adapter.setFragmentFactory(position -> mockFragments[position]);
		for (int i = 0; i < mockCount; i++) {
			savedState.putInt("android:pager:fragment:" + adapter.getItemId(i), i);
			when(mockFragmentManager.getFragment(savedState, "android:pager:fragment:" + adapter.getItemId(i))).thenReturn(i == 0 ? mockFragments[i] : null);
		}
		// Act:
		adapter.restoreState(savedState, Adapter.class.getClassLoader());
		// Assert:
		verify(mockFragments[0], times(1)).setMenuVisibility(false);
		verify(mockFragments[1], times(0)).setMenuVisibility(anyBoolean());
		final FragmentTransaction mockTransaction = mock(FragmentTransaction.class);
		when(mockFragmentManager.beginTransaction()).thenReturn(mockTransaction);
		// Verify that during instantiation process the fragment saved states are attached to proper fragments.
		for (int i = 0; i < mockCount; i++) {
			final Fragment fragment = (Fragment) adapter.instantiateItem(container, i);
			if (i == 0) {
				verify(mockFragments[i], times(0)).setInitialSavedState(any(Fragment.SavedState.class));
				verify(mockTransaction, times(0)).add(CONTAINER_ID, fragment, adapter.makeItemTag(CONTAINER_ID, i));
			} else {
				verify(mockFragments[i], times(1)).setInitialSavedState(mockFragmentStates[i]);
				verify(mockTransaction, times(1)).add(CONTAINER_ID, fragment, adapter.makeItemTag(CONTAINER_ID, i));
			}
		}
		verify(mockFragmentManager).beginTransaction();
		return mockTransaction;
	}

	@Test public void testRestoreEmptyState() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act:
		adapter.restoreState(Bundle.EMPTY, Adapter.class.getClassLoader());
		// Assert:
		verify(mockFragmentManager, times(0)).getFragment(any(Bundle.class), anyString());
	}

	@Test public void testRestoreNullState() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final FragmentStatePagerAdapter adapter = new Adapter(mockFragmentManager);
		// Act:
		adapter.restoreState(null, Adapter.class.getClassLoader());
	}

	private static final class Adapter extends FragmentStatePagerAdapter {

		private FragmentFactory fragmentFactory = position -> mock(Fragment.class);

		Adapter(final FragmentManager fragmentManager) {
			super(fragmentManager);
		}

		void setFragmentFactory(final FragmentFactory factory) {
			this.fragmentFactory = factory;
		}

		@Override public int getCount() {
			return 1;
		}

		@SuppressWarnings("ConstantConditions")
		@Override @NonNull public Fragment getItem(final int position) {
			return fragmentFactory == null ? null : fragmentFactory.createFragment(position);
		}
	}

	private interface FragmentFactory {

		@NonNull Fragment createFragment(int position);
	}

	public static final class TestFragment extends Fragment {

		@Override @NonNull public View onCreateView(
				@NonNull final LayoutInflater inflater,
				@Nullable final ViewGroup container,
				@Nullable final Bundle savedInstanceState
		) {
			return new View(inflater.getContext());
		}
	}
}
