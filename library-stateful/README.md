Pager-Adapters-Stateful
===============

This module contains `PagerAdapter` implementation that **saves and restores state** of its `Fragment`
items when they are destroyed and re-created.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Apager-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Apager-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:pager-adapters-stateful:${DESIRED_VERSION}@aar"

_depends on:_
[pager-adapters-core](https://bitbucket.org/android-universum/pager-adapters/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [FragmentStatePagerAdapter](https://bitbucket.org/android-universum/pager-adapters/src/main/library-stateful/src/main/java/universum/studios/android/pager/adapter/FragmentStatePagerAdapter.java)